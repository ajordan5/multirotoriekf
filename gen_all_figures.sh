# generate speed figure
printf "Starting speed figure..."
python figure_speed.py --data data/data.npz -b -n 350

# generate convergence figure
printf "\nStarting convergence figure..."
python figure_conv.py --data data/data.npz -b -n 10

# generate data for long-term covariance figure
printf "\nGenerating accuracy data..."
declare -a arr=(0.5 1.0 1.5 2)
for i in "${arr[@]}"
do
    python data_gen_accur.py --data data/long1_fair_noise.pkl -b -n 1 -c $i
done
printf "\nStarting accuracy figure..."
# make long-term covariance figure
python figure_accur.py --data data/long1_fair_noise.pkl -s 5
