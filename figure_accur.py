from src.system import UAV
from src.iekf import IEKF
from src.qekf import QEKF

import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm, block_diag
from scipy.spatial.transform import Rotation

import argparse
import matplotlib.pyplot as plt
import matplotlib
from tqdm import tqdm
import seaborn as sns
import pandas as pd

from multiprocessing import Pool
from functools import partial
import os
import pickle

def error(x, x0):
    return np.abs( (x - x0) ).mean(axis=0)
        
def calc_pitch(x):
    return -np.arcsin(x[:,2,0])*180/np.pi

def calc_roll(x):
    return np.arctan2(x[:,2,1], x[:,2,2])*180/np.pi

def calc_error(x, x_inv, results, n=1):
    x0 = np.expand_dims(results['x0'], 0)
    iekf = results['iekf']
    qekf = results['qekf']
    iekf_inv = inv(iekf)
    qekf_inv = inv(qekf)

    vals = []
    vals.append( ['pitch','RInEKF', error(calc_pitch(x[0:1]),   calc_pitch(x0)), 
                                    error(calc_pitch(x[-n:]),   calc_pitch(iekf[-n:]))] )
    vals.append( ['roll', 'RInEKF', error(calc_roll(x[0:1]),    calc_roll(x0)), 
                                    error(calc_roll(x[-n:]),    calc_roll(iekf[-n:]))] )
    vals.append( ['vel',  'RInEKF', np.linalg.norm(error( x_inv[0:1,:3,3], x0[0:1,:3,3])),
                                    np.linalg.norm(error( x_inv[-n:,:3,3], iekf_inv[-n:,:3,3]))] )
    vals.append( ['z',    'RInEKF', error( x[0:1,2,4],   x0[0:1,2,4]),
                                    error( x[-n:,2,4], iekf[-n:,2,4])] )

    vals.append( ['pitch','QEKF',   error(calc_pitch(x[0:1]),   calc_pitch(x0)), 
                                    error(calc_pitch(x[-n:]), calc_pitch(qekf[-n:]))] )
    vals.append( ['roll', 'QEKF',   error(calc_roll(x[0:1]),   calc_roll(x0)), 
                                    error(calc_roll(x[-n:]), calc_roll(qekf[-n:]))] )
    vals.append( ['vel',  'QEKF',   np.linalg.norm(error( x_inv[0:1,:3,3], x0[0:1,:3,3])),
                                    np.linalg.norm(error( x_inv[-n:,:3,3], qekf_inv[-n:,:3,3]))] )
    vals.append( ['z',    'QEKF',   error( x[0:1,2,4],   x0[0:1,2,4]),
                                    error( x[-n:,2,4], qekf[-n:,2,4])] )

    return vals

def main(data, sec):
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42

    ############################     CALCULATE ERRORS    ############################
    # get actual data
    loaded_data = pickle.load(open(data, 'rb'))
    x = loaded_data['x']
    x_inv = inv(x)

    vals =[]
    n = 1000 * sec
    data_name = os.path.splitext(os.path.basename(data))[0]
    name = f"accur_{data_name}_cov"
    dirs = [d for d in os.listdir('results') if name in d]
    dirs = [os.path.join('results', d) for d in dirs]
    for dir in tqdm(dirs):
        cov = float(dir.split('_')[-1][-3:])
        if cov <= 2:
            for file in os.listdir(dir):
                if 'run' in file:
                    loaded_data = pickle.load(open(os.path.join(dir, file), 'rb'))
                    temp = calc_error(x, x_inv, loaded_data, n=n)
                    for t in temp:
                        t.extend([file, cov])
                    vals.extend( temp )

    df = pd.DataFrame(vals, columns=['Type', 'Algo', 'InitError', 'FinalError', 'file', 'cov'])
    df['dummy'] = 'dummy'

    # temp = df[df['Type'] == 'pitch']
    # print(temp.head(16))    

    ############################     SETUP AXIS    ############################
    sns.set(context='paper', style='whitegrid', font_scale=0.8)
    palette = sns.color_palette()

    plt.figure(1, figsize=(8, 1.8))
    bottom = 0.18
    height = 0.7
    wspace = [0.058, 0.038, 0.06, 0.06, 0.01]
    width = (1 - sum(wspace)) / 4
    ax = []
    ax_hist =[]
    for i in range(4):
        ax.append( plt.axes([width*i+sum(wspace[:i+1]), bottom, width, height]) )


    ############################     PLOT     ############################
    s = 5
    alpha = 0.9
    sns.boxplot(data=df[df['Type']=='pitch'], x='cov', y='FinalError', hue='Algo', ax=ax[0])
    sns.boxplot(data=df[df['Type']=='roll'],  x='cov', y='FinalError', hue='Algo', ax=ax[1])
    sns.boxplot(data=df[df['Type']=='z'],     x='cov', y='FinalError', hue='Algo', ax=ax[2])
    sns.boxplot(data=df[df['Type']=='vel'],   x='cov', y='FinalError', hue='Algo', ax=ax[3])

    ############################     CLEAN UP PLOT    ############################

    # set plot titles
    ax[0].set_title("Pitch",         weight='bold')
    ax[1].set_title("Roll",          weight='bold')
    ax[2].set_title("Position-Z",    weight='bold')
    ax[3].set_title("Mag. Velocity", weight='bold')

    # y labels
    ax[0].set_ylabel("Final 5s Mean Abs. Error \n (degrees)", weight='bold')
    ax[1].set_ylabel("")
    ax[2].set_ylabel("(meters)", weight='bold')
    ax[3].set_ylabel("(m/sec)", weight='bold')

    # x labels
    ax[0].set_xlabel('Initial Std. Scale', weight='bold')
    ax[1].set_xlabel('Initial Std. Scale', weight='bold')
    ax[2].set_xlabel('Initial Std. Scale', weight='bold')
    ax[3].set_xlabel('Initial Std. Scale', weight='bold')

    # set up legends
    for a in ax_hist:
        a.legend([],[], frameon=False)
    for a in ax[:-1]:
        a.legend([],[], frameon=False)
    handles, labels = ax[-1].get_legend_handles_labels()
    ax[-1].legend(handles=handles[:], labels=labels[:])

    for a in ax:
        a.tick_params('y', pad=-3)
        a.tick_params('x', pad=-2)

    dir = 'figures'
    os.makedirs(dir, exist_ok=True)
    plt.savefig(os.path.join(dir, f'accur_{data_name}_s{sec}.pdf'), dpi=1000)

    for a in ax:
        a.set_yscale('log')

    plt.savefig(os.path.join(dir, f'{data_name}_s{sec}_log.pdf'), dpi=1000)
    plt.show()

if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="UAV IEKF")
    parser.add_argument("-d", "--data", type=str, default="figures/accur_data_15.pkl", help="File to read data from. Default accur_data_15.npz.")
    parser.add_argument("-s", "--sec", type=int, default="10", help="Number of seconds to average end of run.")
    args = vars(parser.parse_args())
    main(**args)