import numpy as np
import sys
sys.path.append('.')
from scipy.linalg import expm
from src.iekf import IEKF
from src.system import UAV

def test_dimensions():
    """Test basic functionality of gps update. Make sure dimensions are correct"""
    
    from src.constants import Q, R, dvl_p, dvl_r

    #make system
    sys = UAV(Q, R, "./data/data.npz", dvl_p, dvl_r)
    init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 1, 1, 1, .1, .1, .1])**2
    start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
    x0 = expm( sys.carat( start ) ) @ sys.data['x'][0]
    iekf = IEKF(sys, x0, init_cov, left=True)

    # Run update with gps measurment
    gps_mock = np.array([[15,250,-100], [.05, .25, -.5]])
    iekf.update_gps(gps_mock, False)