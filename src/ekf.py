import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm
from tqdm import tqdm

class EKF:
    def __init__(self, system, mu_0, sigma_0):
        """The Extended Kalman Filter for a multirotor utilizing an IMU, Altimeter and GPS Velocity measurement

        Args:
            system      (class) : The system to run the iEKF on. It will pull Q, R, f, h from this.
            mu0     (nx1 array) : Initial starting point of system
            sigma0  (nxn array) : Initial covariance of system"""
        if mu_0.shape == (9,):
            mu_0 = expm( system.carat(mu_0) )

        self.sys = system
        self.mus = [mu_0]
        self.sigmas = [sigma_0]

        self.biass = [np.zeros((2,3))]
    
    # These 3 properties are helpers. Since our mus and sigmas are stored in a list
    # it can be a pain to access the last one. These provide "syntactic sugar"
    @property
    def mu(self):
        return self.mus[-1]

    @property
    def sigma(self):
        return self.sigmas[-1]

    @property
    def bias(self):
        return self.biass[-1]


    def predict(self, u, dt):
        """Runs prediction step of EKF.

        Args:
            u       (k ndarray) : control taken at this step

        Returns:
            mu    (nxn ndarray) : Propagated state
            sigma (nxn ndarray) : Propagated covariances"""
        
        #get mubar and sigmabar
        u = np.flip(u.reshape(2,3), 0)
        mu_bar = self.sys.f(self.mu, u, dt, self.bias)

        omega = u[0] - self.bias[0]
        a = u[1] - self.bias[1]

        A = np.zeros((9,9))
        A[0,3] = 1
        A[1,4] = 1
        A[2,5] = 1 
        A[3,7] = a[2]
        A[4,6] = -a[2]
        
        state = self.mu

        G = np.zeros((9,6))
        G[3,2] = state[7]
        G[4,2] = -state[6]
        G[5,2] = 1
        G[6,3] = 1
        G[7,4] = 1
        G[8,5] = 1

        sigma_bar = self.sigma + dt*(A@self.sigma + self.sigma@A.T + G@ self.sys.Q_ekf @ G.T) 

        #save for use later
        self.last_u = u
        self.mus.append( mu_bar )
        self.biass.append( self.bias )
        self.sigmas.append( sigma_bar )

        return mu_bar, sigma_bar

    def update_altitude(self, z):
        """Runs correction step of iEKF.

        Args:
            z (m ndarray): altimeter measurement at this step measured as negative up => z=-altitude

        Returns:
            mu    (nxn ndarray) : Corrected state
            sigma (nxn ndarray) : Corrected covariances"""
        # make H matrices
        b = np.asarray([[0, 0, -1, 0, 0, 0, 0, 0, 0]])

        z_hat = b @ self.mu
        R = self.sys.R[6,6]
        K = self.sigma @ b.T /(b @ self.sigma @ b.T + R)
        self.mus[-1] = self.mu + K@(z-z_hat)
        self.sigmas[-1] = (np.eye(len(self.mu)) -K@b)@self.sigma
        return self.mu, self.sigma

    def update_gps(self, z):
        """Runs correction step of iEKF with gps velocity.

           Args:
               z           (m ndarray): measurement at this step
               sim_bias    (bool): decision flag for calculating bias

           Returns:
               mu    (nxn ndarray) : Corrected state
               sigma (nxn ndarray) : Corrected covariances"""

        z = z.reshape((-1,1))



        b = np.asarray([[0, 0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 1, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 1, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 1, 0, 0, 0]])



        z_hat = b @ self.mu
        R = self.sys.R[:6,:6]
        K = self.sigma @ b.T @ np.linalg.inv(b @ self.sigma @ b.T + R)
        self.mus[-1] = self.mu + K@(z-z_hat)
        self.sigmas[-1] = (np.eye(len(self.mu)) - K@b)@self.sigma
        return self.mu, self.sigma

    def iterate(self, u, tu, z1, tz1, z2, tz2, z3, tz3, t, sim_bias=False, use_tqdm=True):
        """Given a sequence of observation, iterate through EKF
        
        Args:
            us (txk ndarray) : controls for each step, each of size k
            zs (txm ndarray) : measurements for each step, each of size m
            
        Returns:
            mus    (txnxn ndarray) : resulting states
            sigmas (txnxn ndarray) : resulting sigmas"""
        ui = 0
        z1i = 0
        z2i = 0
        t_range = t #np.linspace(0, self.sys.T/self.sys.Hz, self.sys.T)
        
        # append one at the end, so after they're done they won't keep running
        tu = np.append(tu, t_range[-1]+1)
        tz1 = np.append(tz1, t_range[-1]+1)
        tz2 = np.append(tz2, t_range[-1]+1)

        dt = 0
        for t in tqdm(t_range, disable=(not use_tqdm)):
            if tu[ui] <= t:
                if ui != 0:
                    dt = tu[ui] - tu[ui-1]
                self.predict(u[ui], dt)
                if ui < len(tu)-1:
                    ui += 1
            if tz1[z1i] <= t:
                self.update_gps(np.array([z3[z1i],z1[z1i]]))
                if z1i < len(tz1)-1:
                    z1i += 1
            if tz2[z2i] <= t:
                self.update_altitude(z2[z2i])
                if z2i < len(tz2)-1:
                    z2i += 1
            

        return np.array(self.mus)[0:], np.array(self.biass)[1:], np.array(self.sigmas)[0:]