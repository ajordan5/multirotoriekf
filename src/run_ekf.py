from system import UAV
from ekf import EKF
import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import inv
from scipy.linalg import expm
from scipy.spatial.transform import Rotation
import argparse

def main(data, left, sim_bias, plot, verbose):
    np.set_printoptions(suppress=True, formatter={'float_kind':f'{{:0.{verbose}f}}'.format}) 

    from constants import Q, R, imuHz, gpsHz, altHz, Q_ekf

    #make system
    sys = UAV(Q, R, data, Q_ekf)
    t = sys.T

    # generate data
    (x, t), u, (bias, _), z1, z2, z3 = sys.gen_data(t, imuHz, gpsHz, altHz, noise=False, sim_bias=sim_bias)

    # Run the iekf
    if sim_bias:
        init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 1, 1, 1, .1, .1, .1,
                            .005, .005, .005, .05, .05, .05])**2
    else:
        init_cov = np.diag([np.pi/6, np.pi/6, np.pi/6, 1, 1, 1, .1, .1, .1])
    start = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
    x0 = np.random.multivariate_normal(mean=np.zeros(9), cov=init_cov[:9,:9])
    x0= x0.reshape(-1,1)
    ekf = EKF(sys, x0, init_cov)
    x_result, bias_result, sigmas = ekf.iterate(*u, *z1, *z2, *z3, t, sim_bias)

    # Plot everything as requested
    if plot != '':
        plt.rcParams.update({'font.size': 16})
        # set up our figure
        rows = len(plot) if 'b' not in plot else len(plot)+1
        fig, ax = plt.subplots(rows, 3, figsize=(8, rows*2+2))
        fig_err, ax_err = plt.subplots(rows, 3, figsize=(8, rows*2+2))
        if rows == 1:
            ax = ax.reshape((1,3))

        # invert stuff if'll we'll need local frame later
        """if 'p' in plot or 'v' in plot:
            x_inv = inv(x)
            x_result_inv = inv(x_result)
            x_inv[:,0:3,3:5] *= -1
            x_result_inv[:,0:3,3:5] *= -1"""

        i = 0
        # iterate through plotting everything
        for p in plot:
            # plot angles
            if p == "a":
                ax[i,0].set_ylabel("Attitude")
                ax[i,0].set_title("Pitch")
                ax_err[i,0].set_ylabel("Attitude Error")
                ax_err[i,0].set_title("Pitch")
                ax[i,0].plot(t, x[:,6], label="Actual")
                ax[i,0].plot(t, x_result[:,7,0], label="Predicted")
                pitch_err = x[:,6] - x_result[:,7,0]
                ax_err[i,0].plot(t, pitch_err)
                ax[i,1].set_title("Roll")
                ax_err[i,1].set_title("Roll")
                ax[i,1].plot(t, x[:,7], label="Actual")
                ax[i,1].plot(t, x_result[:,6,0], label="Predicted")
                roll_err =  x[:,7] - x_result[:,6,0]
                ax_err[i,1].plot(t, roll_err)
                ax[i,2].set_title("Yaw (Unobservable)")
                ax[i,2].plot(t, x[:,8], label="Actual")
                ax[i,2].plot(t, x_result[:,8,0], label="Predicted")
                i += 1

            # plot velocity
            if p == 'v':
                ax[i,0].set_ylabel("Velocity")
                ax[i,0].set_title("X (global)")
                ax[i,1].set_title("Y (global)")
                ax[i,2].set_title("Z (global)")
                ax_err[i,0].set_ylabel("Velocity Error")
                ax_err[i,0].set_title("X (global)")
                ax_err[i,1].set_title("Y (global)")
                ax_err[i,2].set_title("Z (global)")
                
                for j in range(3):
                    ax[i,j].plot(t, x[:,3+j], label="Actual")
                    ax[i,j].plot(t, x_result[:,j+3,0], label="Predicted")
                    error = x[:,3+j] - x_result[:,j+3,0]
                    ax_err[i,j].plot(t, error)
                i += 1
                
            # plot position
            if p == 'p':
                ax[i,0].set_ylabel("Position")
                ax[i,0].set_title("X (global)")
                ax_err[i,0].set_ylabel("Position Error")
                ax_err[i,0].set_title("X (global)")
                ax[i,0].plot(t, x[:,0], label="Actual")
                ax[i,0].plot(t, x_result[:,0,0], label="Predicted")
                x_err = x[:,0] - x_result[:,0,0]
                ax_err[i,0].plot(t, x_err)
                ax[i,1].set_title("Y (global)")
                ax_err[i,1].set_title("Y (global)")
                ax[i,1].plot(t, x[:,1], label="Actual")
                ax[i,1].plot(t, x_result[:,1,0], label="Predicted")
                y_err = x[:,1] - x_result[:,1,0]
                ax_err[i,1].plot(t, y_err)
                ax[i,2].set_title("Z (global)")
                ax_err[i,2].set_title("Z (global)")
                ax[i,2].plot(t, x[:,2], label="Actual")
                ax[i,2].plot(t, x_result[:,2,0], label="Predicted")
                z_err = x[:,2] - x_result[:,2,0]
                ax_err[i,2].plot(t, z_err)
                i += 1

            
            # plot bias
            # if p == 'b':
            #     for j in range(3):
            #         ax[i,j].set_ylabel("Gyro Bias")
            #         ax[i,j].plot(t, bias[:,0,j], label="Actual")
            #         ax[i,j].plot(t, bias_result[:,0,j], label="Result")
            #         ax[i+1,j].set_ylabel("Accel Bias")
            #         ax[i+1,j].plot(t, bias[:,1,j], label="Actual")
            #         ax[i+1,j].plot(t, bias_result[:,1,j], label="Result")
            #     i += 2
            
        ax[-1,2].legend(loc='best')
        fig.tight_layout()
        fig_err.tight_layout()
        plt.setp(ax_err, xlim=[0,0.5])
        plt.show()

if __name__ == "__main__":
    # Parse through arguments
    parser = argparse.ArgumentParser(description="UAV IEKF")
    parser.add_argument("-d", "--data", type=str, default="./data/data4.npz", help="File to read data from. Default data.npz.")
    parser.add_argument("-l", "--left", action="store_true", help="Include if LIEKF should be used. Defaults to RIEKF.")
    parser.add_argument("-b", "--sim_bias", action="store_true", help="Include if bias should be simulated and tracked.")
    parser.add_argument("-p", "--plot", type=str, default="avp", help="Each char is a state to plot. a (angle), v (velocity), p (position), b (bias). Default avp.")
    parser.add_argument("-v", "--verbose", type=int, default="3", help="Precision for printing. Default 3")
    args = vars(parser.parse_args())
    main(**args)