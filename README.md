# Quadrotor IEKF
Implementation of the IEKF on quadrotors. The IEKF takes in IMU measurements as controls and GPS(position and velocity) and pressure altitude measurements as measurements.

This is a close adaptation from the Invariant Kalman Filter defined by Potokar et al. in his paper for Underwater AUVs. This package was created as a class project where we sought to replicate their filter and adapt it for quadrotors. As such, the work found here was started by forking their repository and making the appropriate changes for a quadrotor a huge thanks to them for their work!

Paper: https://frostlab.byu.edu/0000017a-5569-d857-adfb-7ff9c87f0000/inekf-underwater-pdf

Code: https://bitbucket.org/frostlab/underwateriekf/src/main/

## Overview of Source Files
`system.py` holds all the info about dynamics, adding noise to the simulation, etc.

`iekf.py` holds the actual InEKF.

`ekf.py` holds the actual EKF defined in Dr. Randal Beard's "Quadrotor Dynamics and Control rev 01" https://scholarsarchive.byu.edu/facpub/1325/.

`run_iekf.py`/`run_ekf.py` runs their respective filters. Similarly to the simulation, it can take a bunch of flags. Run `python run_blank.py --help` to see them.


## Stored Data
There is some data stored that was used in the paper. It is as follows:

`data/data(1-4).npz` are compiled data sets of a quadrotor flying an autonomous square trajectory mission in the Airsim simulator. Contains truth data along with gps position and velocity, altimeter and imu simulated measurements with noise parameters similar to those found in `constants.py`. Note that the datasets in `data3.npz` and `data4.npz` have GPS data defined in an NED frame relative to the quadrotors relative position and altimeter given in AGL (above ground level) for simplicity. These datasets are recommended.

A video demonstrating an example of a flight from which data was recored can be viewed here: https://drive.google.com/file/d/16DKi7APaIt8H6frPhFbOrFWuY-iLkqzl/view
